NOTE
This is strickly notes that were written to help others setup a Jitsi Meet server using docker (compose).
Most of these steps were lifted from the official Jisti Meet GitHub.

https://github.com/jitsi/docker-jitsi-meet

This might one day fall out-of-date. Please read their official documentation.

# How to install Jitsi Meet from a docker container using docker-compose #

This was written and tested on Ubuntu 20.04. YMMV

Also this was written assuming that no other docker containers are running or are planning to be ran along side Jitsi Meet.

## Install docker and docker-compose ##

0.  Install docker via apt

```sh
sudo apt install docker.io docker-compose
```

## Download and setup jitst-meet from GitHub ##

We will use the .env on this Gitlab page to further customize the docker container.

1.  Remove old config files first, if this is your first time. You should expect an error message.

```sh
sudo mv ~/.jitsi-meet-cfg/ ~/.jitsi-meet-cfg-OLD/
```

2.  Create fresh config folders for jitsi-meet to populate once docker image is working

```sh
mkdir -p ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody,jicofo,jvb}
```

```sh
cd ~
```

3.  [Download the latest Jitsi-meet docker image.](https://github.com/jitsi/docker-jitsi-meet/releases/latest)

__This will likely be outdated__, so please make sure you **update the URL** in the curl command.

```sh
curl --location "https://github.com/jitsi/docker-jitsi-meet/archive/refs/tags/stable-7210-2.tar.gz" --output ~/stable-7210-2.tar.gz
tar --extract --file=stable-7210-2.tar.gz --directory ~
cd ~/docker-jitsi-meet-stable-7210-2/
```

4.  Now that you are in the docker-jitsi-meet folder, pull down the working .env file.

```sh
curl -L "https://gitlab.com/ABQLUG/Install-Jitsi-Meet-with-Docker-Compose/-/raw/master/.env?inline=false" -o .env
```

5.  By default the .env has unsafe passwords in use. You can enhance the security by doing the following:

```sh
bash gen-passwords.sh
```

6.  Now you can edit the .env file. That will configure jitsi-meet to your liking. The original .env file can be found here: <https://github.com/jitsi/docker-jitsi-meet/blob/master/env.example>

```sh
nano .env
```

7.  By default, the .env will have Let's Encrypt disabled. This is because if you issue too many new certificates, you will not be able to bypass the rate limit. One way of handling this is to keep the keys in a safe location. Otherwise, enable it when you use it. And don't delete the keys more than five times in a seven day span.
  
| Change on `.env`                         |
| ---------------------------------------- |
| PUBLIC_URL=https://target.domain.com      |
| # LETSENCRYPT_USE_STAGING=1               |
| ENABLE_LETSENCRYPT=1                      |
| LETSENCRYPT_DOMAIN=target.domain.com      |
| LETSENCRYPT_EMAIL=email-for-le@domain.com |

8.  You should now be able to deploy the docker image for jitsi-meet. You can use this to look for errors before deploying the docker image as a daemon.

```sh
docker-compose up
```

9.  Verify that you can load jitst-meet through your brower.

```sh
https://target.domain.com/
```

10. Once you are happy with the Jitsi-meet docker image. You can now run the image under a daemon.

```sh
docker-compose up --detach
```

11. If you want to stop the docker image.

```sh
docker-compose down
```

12. Verify that the docker image has stopped.

```sh
docker ps
```

## Adding user authentication

First, grab the full NAME of the Prosody docker image

```sh
docker ps
docker exec -it docker-jitsi-meet-stable-7210-2_prosody_1 prosodyctl --config /config/prosody.cfg.lua register TheDesiredUsername meet.jitsi TheDesiredPassword
```

FYI, you can also use JWT or LDAP authentication.

## (Optional) Allow Jibri to record locally ##

At the time of writing, jibri seems to not be working correctly. Even when properly configured.

These are the steps you need to take, given that `.env` is still configured to allow jibri to work.

1.  First, install linux-image-extra-virtual

```sh
sudo apt update
sudo apt install linux-image-extra-virtual
```

2.  Second, add this file

```sh
echo "options snd-aloop enable=1,1,1,1,1 index=0,1,2,3,4" | sudo tee /etc/modprobe.d/alsa-loopback.conf
```

3.  Third, append this file

```sh
echo "snd-aloop" | sudo tee -a /etc/modules
```

4.  Stop any working running on the server, and reboot

```sh
sudo systemctl reboot
```

5.  Test to see if the `snd_aloop` module is currently running

```sh
lsmod | grep snd_aloop
```


### Troubleshooting steps:

*  Make sure that DNS is setup correctly

```sh
dig @nameserverfor.domain.com target.domain.com +short`
```

```
1.2.3.4
```

*  Make sure the server you deploying the docker image has the same IP address

```sh
curl ifconfig.me
```

```sh
1.2.3.4
```
